package server

import (
	"fmt"
	"frontend/models"
	"html/template"
	"net/http"
	"time"
)

func (s *server) driverHandler(w http.ResponseWriter, r *http.Request) {
	driver := models.DriverList[1]

	templ, _ := template.ParseFiles(
		"templates/driver.html",
		"templates/header.html",
		"templates/footer.html",
	)
	err := templ.Execute(w, driver)
	if err != nil {
		s.logger.Error(err.Error())
	}
}

func (s *server) driverListHandler(w http.ResponseWriter, r *http.Request) {
	drivers := models.DriverList

	templ, _ := template.ParseFiles(
		"templates/driverlist.html",
		"templates/header.html",
		"templates/footer.html",
	)
	err := templ.Execute(w, drivers)
	if err != nil {
		s.logger.Error(err.Error())
	}
}

func (s *server) getDriver(w http.ResponseWriter, r *http.Request) {
	driver := s.getDriverByID("HAM")

	templ, _ := template.ParseGlob("templates/*.html")
	err := templ.Execute(w, driver)
	if err != nil {
		s.logger.Error(err.Error())
	}
}

func (s *server) getDriverByID(ID string) *models.Driver {
	for _, driver := range models.DriverList {
		if driver.DriverID == ID {
			return &driver
		}
	}
	return nil
}

func (s *server) emailServiceHandler(w http.ResponseWriter, r *http.Request) {
	templ, _ := template.ParseFiles(
		"templates/email-service-form.html",
		"templates/header.html",
		"templates/footer.html",
	)

	if r.Method != http.MethodPost {
		err := templ.Execute(w, nil)
		if err != nil {
			s.logger.Error(err.Error())
		}
		return
	}

	newUser := models.User{
		FirstName:  r.FormValue("firstname"),
		LastName:   r.FormValue("lastname"),
		Email:      r.FormValue("email"),
		SignupTime: time.Now(),
	}

	err := s.repo.PostNewUser(&newUser)
	if err != nil {
		return
	}

	// Todo: Add confirmation email service

	fmt.Println(newUser)

	err = templ.Execute(w, struct{ Success bool }{true})
	if err != nil {
		s.logger.Error(err.Error())
	}
}
