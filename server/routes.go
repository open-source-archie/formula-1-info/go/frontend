package server

func (s *server) registerRoutes() {
	s.router.HandleFunc("/driver", s.driverHandler).Methods("GET")
	s.router.HandleFunc("/drivers", s.driverListHandler).Methods("GET")
	s.router.HandleFunc("/emailservice", s.emailServiceHandler)
}
