package repositories

import (
	"database/sql"
	"frontend/models"
	"go.uber.org/zap"
)

type UserPoster interface {
	PostNewUser(user *models.User) error
}

// repository is a custom type which wraps the sql.DB connection pool REPO
type repository struct {
	logger *zap.Logger
	DB     *sql.DB
}

func NewRepository(logger *zap.Logger, db *sql.DB) *repository {
	return &repository{
		logger: logger,
		DB:     db,
	}
}

func (repo *repository) PostNewUser(user *models.User) error {
	query := "INSERT INTO f1_info.users (first_name, Last_name, email, signup_date) VALUES ($1, $2, $3, $4)"

	_, err := repo.DB.Exec(query, user.FirstName, user.LastName, user.Email, user.SignupTime)

	defer repo.DB.Close()

	return err
}
