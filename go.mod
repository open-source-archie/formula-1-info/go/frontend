module frontend

go 1.17

require (
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.4
	go.uber.org/zap v1.19.1
	gopkg.in/yaml.v2 v2.4.0
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
